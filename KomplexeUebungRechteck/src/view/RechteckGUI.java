package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.border.MatteBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class RechteckGUI extends JFrame
{

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					RechteckGUI frame = new RechteckGUI();
					frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public RechteckGUI()
	{
		setTitle("Rechtecke erstellen - GUI");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(10, 100));
		contentPane.add(panel, BorderLayout.NORTH);

		JLabel lblNewLabel = new JLabel("Rechtecke kreieren");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblNewLabel.setBorder(new MatteBorder(0, 0, 2, 0, (Color) new Color(0, 0, 0)));
		panel.add(lblNewLabel);

		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.Y_AXIS));

		JPanel panel_3 = new JPanel();
		panel_1.add(panel_3);

		JLabel lblNewLabel_1 = new JLabel("Position X");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel_3.add(lblNewLabel_1);

		textField = new JTextField();
		textField.setPreferredSize(new Dimension(2, 22));
		panel_3.add(textField);
		textField.setColumns(10);

		JPanel panel_4 = new JPanel();
		panel_1.add(panel_4);

		JLabel lblNewLabel_2 = new JLabel("Position Y");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel_4.add(lblNewLabel_2);

		textField_1 = new JTextField();
		textField_1.setPreferredSize(new Dimension(7, 22));
		panel_4.add(textField_1);
		textField_1.setColumns(10);

		JPanel panel_5 = new JPanel();
		panel_1.add(panel_5);

		JLabel lblNewLabel_3 = new JLabel("Breite");
		lblNewLabel_3.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel_5.add(lblNewLabel_3);

		textField_2 = new JTextField();
		textField_2.setPreferredSize(new Dimension(7, 22));
		panel_5.add(textField_2);
		textField_2.setColumns(10);

		JPanel panel_6 = new JPanel();
		panel_1.add(panel_6);

		JLabel lblNewLabel_4 = new JLabel("H\u00F6he");
		lblNewLabel_4.setFont(new Font("Tahoma", Font.PLAIN, 18));
		panel_6.add(lblNewLabel_4);

		textField_3 = new JTextField();
		textField_3.setPreferredSize(new Dimension(7, 22));
		panel_6.add(textField_3);
		textField_3.setColumns(10);

		JPanel panel_2 = new JPanel();
		panel_2.setPreferredSize(new Dimension(10, 100));
		contentPane.add(panel_2, BorderLayout.SOUTH);

		JPanel panel_7 = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel_7.getLayout();
		flowLayout.setVgap(25);
		flowLayout.setHgap(15);
		panel_7.setPreferredSize(new Dimension(150, 100));
		panel_2.add(panel_7);

		JButton btnNewButton = new JButton("Erstellen");
		btnNewButton.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseReleased(MouseEvent e)
			{
				if (!textField.getText().equals("") && !textField_1.getText().equals("") && !textField_2.getText().equals("") && !textField_3.getText().equals(""))
				{
					BunteRechteckeController brc = new BunteRechteckeController();
					brc.add(new Rechteck(Integer.valueOf(textField.getText()), Integer.valueOf(textField_1.getText()),
							Integer.valueOf(textField_2.getText()), Integer.valueOf(textField_3.getText())));
					
					System.out.println(brc.toString());

					textField.setText("");
					textField_1.setText("");
					textField_2.setText("");
					textField_3.setText("");
				}
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNewButton.setPreferredSize(new Dimension(150, 30));
		btnNewButton.setHorizontalTextPosition(SwingConstants.CENTER);
		panel_7.add(btnNewButton);
	}

}
