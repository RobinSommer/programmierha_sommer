package view;

import java.awt.*;
import java.util.LinkedList;

import javax.swing.*;
import controller.BunteRechteckeController;
import model.Rechteck;

public class Zeichenflaeche extends JPanel
{
	final BunteRechteckeController CONTROLLER;

	public Zeichenflaeche(BunteRechteckeController brc1)
	{
		CONTROLLER = brc1;
	}

	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		LinkedList<Rechteck> rechtecke = CONTROLLER.getRechtecke();
		
		for (int i = 0; i < rechtecke.size(); i++)
		{
			g.setColor(Color.black);
			g.drawRect(rechtecke.get(i).getX(), rechtecke.get(i).getY(), rechtecke.get(i).getBreite(), rechtecke.get(i).getHoehe());
		}
	}
}
