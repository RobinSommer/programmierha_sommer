package controller;

import java.util.*;
import model.Rechteck;

public class BunteRechteckeController
{
	static LinkedList<Rechteck> rechtecke = new LinkedList<Rechteck>();

	public static void main(String[] args)
	{

	}

	public BunteRechteckeController()
	{
		rechtecke = new LinkedList<Rechteck>();
	}

	public void add(Rechteck rechteck_new)
	{
		rechtecke.add(rechteck_new);
	}

	public void reset()
	{
		rechtecke.clear();
	}

	public static LinkedList<Rechteck> getRechtecke()
	{
		return rechtecke;
	}

	@Override
	public String toString()
	{
		return "BunteRechteckeController [rechtecke=" + rechtecke + "]";
	}
	
	public void generiereZufallsRechteck(int anzahl)
	{
		reset();
		for (int i = 0; i < anzahl; i++)
		{
			rechtecke.add(new Rechteck().generiereZufallsRechteck());
		}
	}
}
