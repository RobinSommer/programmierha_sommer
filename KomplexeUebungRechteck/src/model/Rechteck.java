package model;

import java.util.Iterator;
import java.util.Random;

public class Rechteck
{
	Punkt p = new Punkt(0, 0);
	int breite;
	int hoehe;

	public Rechteck()
	{
	}

	public Rechteck(int x, int y, int breite, int hoehe)
	{
		this.p.setX(x);
		this.p.setY(y);
		this.breite = mathabs(breite);
		this.hoehe = mathabs(hoehe);
	}

	public int getX()
	{
		return p.getX();
	}

	public void setX(int x)
	{
		this.p.setX(x);
		;
	}

	public int getY()
	{
		return p.getY();
	}

	public void setY(int y)
	{
		this.p.setY(y);
		;
	}

	public int getBreite()
	{
		return breite;
	}

	public void setBreite(int breite)
	{
		this.breite = mathabs(breite);
	}

	public int getHoehe()
	{
		return hoehe;
	}

	public void setHoehe(int hoehe)
	{
		this.hoehe = mathabs(hoehe);
	}

	public int mathabs(int wert)
	{
		return Math.abs(wert);
	}

	@Override
	public String toString()
	{
		return "Rechteck [x=" + p.getX() + ", y=" + p.getY() + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}

	public boolean enthaelt(int x, int y, Punkt p)
	{
		// Bei zwei Werten, Punkt p = Null
		// Bei einem Punkt, x & y = 0

		Punkt p2 = null;
		if (p == null)
		{
			p2 = new Punkt(x, y);
		}
		if (x == 0 && y == 0)
		{
			p2 = p;
		}

		if (p2.getX() >= 0 && p2.getX() <= breite)
		{
			if (p2.getY() >= 0 && p2.getY() <= hoehe)
			{
				return true;
			}
		}

		return false;
	}

	public boolean enthaelt(Rechteck r_new)
	{
		return breite > 0 && hoehe > 0 && r_new.getBreite() > 0 && r_new.getHoehe() > 0
                && r_new.getX() >= this.p.getX() && r_new.getX() + r_new.getBreite() <= this.p.getX() + this.breite
                && r_new.getY() >= this.p.getY() && r_new.getY() + r_new.getHoehe() <= this.p.getY() + this.hoehe;	
	}
	
	public static Rechteck generiereZufallsRechteck()
	{
		Random ran = new Random();
		
		int breite_new = ran.nextInt(1198 + 1) + 1;
		int hoehe_new = ran.nextInt(998 + 1) + 1;

		int x = 1200 - breite_new;
		int y = 1000 - hoehe_new;
		
		int test_x = ran.nextInt(x);
		int test_y = ran.nextInt(y);
		
		return new Rechteck(test_x, test_y, breite_new, hoehe_new);
		
	}
}
