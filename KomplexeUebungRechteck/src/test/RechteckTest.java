package test;

import java.util.ArrayList;

import controller.BunteRechteckeController;
import model.Rechteck;
import view.Zeichenflaeche;

public class RechteckTest
{
	static Rechteck r0 = new Rechteck(10, 10, 30, 40);
	static Rechteck r1 = new Rechteck(25, 25, 100, 20);
	static Rechteck r2 = new Rechteck(260, 10, 200, 100);
	static Rechteck r3 = new Rechteck(5, 500, 300, 25);
	static Rechteck r4 = new Rechteck(100, 100, 100, 100);

	static Rechteck r5 = new Rechteck();
	static Rechteck r6 = new Rechteck();
	static Rechteck r7 = new Rechteck();
	static Rechteck r8 = new Rechteck();
	static Rechteck r9 = new Rechteck();
	
	static Rechteck r10 = new Rechteck(-4,-5,-50,-200);
	
	static Rechteck r11 = new Rechteck();
	
	static BunteRechteckeController controller0 = new BunteRechteckeController();
	
	static Zeichenflaeche z1 = new Zeichenflaeche(controller0);

	public static void main(String[] args)
	{
		r5.setX(200);
		r5.setY(200);
		r5.setBreite(200);
		r5.setHoehe(200);
		
		r6.setX(800);
		r6.setY(400);
		r6.setBreite(20);
		r6.setHoehe(20);
		
		r7.setX(800);
		r7.setY(450);
		r7.setBreite(20);
		r7.setHoehe(20);
		
		r8.setX(850);
		r8.setY(400);
		r8.setBreite(20);
		r8.setHoehe(20);
		
		r9.setX(855);
		r9.setY(455);
		r9.setBreite(25);
		r9.setHoehe(25);
		
		r11.setX(-10);
	    r11.setY(-10);
	    r11.setBreite(-200);
	    r11.setHoehe(-100);
		
		System.out.println(r0.toString());
		
		controller0.add(r0);
		controller0.add(r1);
		controller0.add(r2);
		controller0.add(r3);
		controller0.add(r4);
		controller0.add(r5);
		controller0.add(r6);
		controller0.add(r7);
		controller0.add(r8);
		controller0.add(r9);
		
		System.out.println(controller0.toString());
		
		System.out.println("");
		System.out.println(r10.toString());
		System.out.println(r11.toString());
		
		System.out.println("");
		System.out.println("Zufälliges Rechteck:");
		
		Rechteck r_random = Rechteck.generiereZufallsRechteck();
		System.out.println(r_random.toString());
		
		System.out.println("");
		System.out.println("50000er Array Test:");
		
		rechteckeTesten();
	}
	
	public static void rechteckeTesten()
	{
		int anzRichtigeRechtecke = 0;
		Rechteck bereich = new Rechteck(0, 0, 1200, 1000);
		Rechteck[] tests = new Rechteck[50000];
		
		for (int i = 0; i < tests.length; i++)
		{
			Rechteck r = new Rechteck().generiereZufallsRechteck();
			tests[i] = r;
			
			if (bereich.enthaelt(r))
			{
				anzRichtigeRechtecke++;
			}
		}

		System.out.println("Es sind " + anzRichtigeRechtecke + " von " + 50000 + " Rechtecken im vorgegebenen Bereich!");
	}
}
